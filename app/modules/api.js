const url = require('url');
const axios = require('axios');
const config = require('config');
const FormData = require('form-data');

const instance = axios.create({
  headers: {
    Authorization: config.get('auth.basic'),
  },
});

const serializeData = (data = {}) => {
  const params = new URLSearchParams();
  Object.keys(data).forEach(key => {
    params.append(key, data[key]);
  });
  return params;
};

const appendFormItem = ({ form, key, value }) => {
  if (Array.isArray(value)) {
    value.forEach(subValue => appendFormItem({ form, key, value: subValue }));
  } else {
    form.append(key, value);
  }
};

const createFormData = (data = {}) => {
  const form = new FormData();
  if (data) {
    Object.entries(data).forEach(([key, value]) => {
      appendFormItem({ form, key, value });
    });
  }
  return form;
};

const processRequest = async request => {
  const result = {};
  try {
    const response = await request();

    const { status, data } = response;

    if (status >= 300 || status < 200 || !data || !data.status) {
      throw new Error(`Server Error. ${response.data.message}`);
    }

    result.response = response;
    result.data = data.response;
  } catch (error) {
    result.error = error;
  }

  if (result.error) {
    let message = result.error;
    try {
      message += ` ${result.error.response.data.message}`;
    } catch (e) {
      // do nothing
    }
    debugger;
    throw new Error(message);
  }

  return result.data;
};

const createResolvePost = apiBaseUrl => async (endpointUrl, body) => {
  const request = async () =>
    await instance.post(
      url.resolve(apiBaseUrl, endpointUrl),
      serializeData(body),
    );
  return await processRequest(request);
};

const createResolvePostFile = apiBaseUrl => async (endpointUrl, body) => {
  // const request = () =>
  //   instance.post(url.resolve(apiBaseUrl, endpointUrl), createFormData(body));
  const request = async () =>
    await instance.post(url.resolve(apiBaseUrl, endpointUrl), body, {
      headers: {
        ...body.getHeaders(),
      },
    });
  return await processRequest(request);
};

exports.apiFeeds = {
  post: createResolvePost(process.env.FEEDS_REST_API_URL),
  postFile: createResolvePostFile(process.env.FEEDS_REST_API_URL),
};

exports.apiDE = {
  post: createResolvePost(process.env.DE_REST_API_URL),
  postFile: createResolvePostFile(process.env.DE_REST_API_URL),
};
