const SockJS = require('sockjs-client');
const Stomp = require('@stomp/stompjs');
const { morphism } = require('morphism');
const { taskSchema } = require('resolvers/_resolver-data-schemas');

let wsConnection = null;

const { shootTaskSub } = require('modules/tasks-subscription');

exports.wsConnect = () => {
  try {
    if (wsConnection) return;

    wsConnection = new SockJS(process.env.DE_WS_UPGRADE_REQUEST);
    const stompClient = Stomp.over(wsConnection);
    stompClient.debug = () => {};

    stompClient.connect(
      {},
      frame => {
        // subscribe to the /geoword/tasks endpoint
        stompClient.subscribe(process.env.DE_WS_RELATIVE_URL, data => {
          try {
            shootTaskSub(morphism(taskSchema, JSON.parse(data.body)));
          } catch (e) {
            // handle error
            debugger;
          }
        });
      },
      err => {
        wsConnection = null;
      },
    );
  } catch (e) {
    debugger;
  }
};
