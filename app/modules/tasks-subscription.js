const { PubSub } = require('apollo-server-express');

const tasksPubsub = (exports.tasksPubsub = new PubSub());
const TASK_UPDATED = (exports.TASK_UPDATED = 'TASK_UPDATED');

exports.shootTaskSub = task => {
  console.log('Task', Object.keys(task));
  tasksPubsub.publish(TASK_UPDATED, task);
};
