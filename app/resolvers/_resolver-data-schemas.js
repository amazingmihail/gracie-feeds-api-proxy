exports.taskSchema = {
  id: 'taskId',
  typeId: 'type.typeId',
  status: iteratee => {
    switch (iteratee.status) {
      case 'Running':
        return 'RUNNING';
      case 'Completed':
        return 'SUCCEED';
      case 'Failed':
        return 'FAILED';
      case 'Waiting':
        return 'QUEUED';
      case 'Cancelling':
        return 'CANCELLING';
      default:
        debugger;
        return null;
    }
  },
  userId: 'userId',
  parameters: 'parameters',
  result: 'result',
  error: {
    message: 'errorMessage',
  },
  timestamps: {
    createdAt: 'createTimeMs',
    startedAt: 'startTimeMs',
    endedAt: 'endTimeMs',
    completedIn: 'runTimeMs',
  },
};
