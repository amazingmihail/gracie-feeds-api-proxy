const { apiFeeds } = require('modules/api');
const { morphism } = require('morphism');

const projectSchema = {
  id: 'id',
  isRunning: 'isRunning',
  name: 'name',
  ttl: 'ttl',
  // pipelineId: "textProcessingPipelineId"
};

exports.projectResolvers = {
  Query: {
    project: async (_, { id: projectId }) => {
      const data = await apiFeeds.post('projects/retrieve', { projectId }
      );
      return morphism(projectSchema, data);
    },

    projects: async () => {
      const data = await apiFeeds.post('projects/list');
      const projects = data.map(project => morphism(projectSchema, project));
      return {
        items: projects,
        itemsTotalCount: projects.length,
      };
    },
  },

  Mutation: {
    addProject: (_, args) => {},
    editProject: (_, args) => {},
    deleteProject: (_, args) => {},
    cloneProject: (_, args) => {},
  },
};
