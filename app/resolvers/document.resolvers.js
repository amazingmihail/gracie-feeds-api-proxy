const { apiDE } = require('modules/api');
const { resolveLanguage } = require('resolvers/language.resolvers');
const FormData = require('form-data');
const { morphism } = require('morphism');
const { taskSchema } = require('./_resolver-data-schemas');

exports.resolveFolder = async args => {
  const { items, itemsTotalCount, ...rest } = await apiDE.post(
    'document/list',
    args,
  );
  return {
    ...rest,
    content: {
      items,
      itemsTotalCount,
    },
  };
};

exports.documentResolvers = {
  FolderContentConnection: {
    __resolveType: async ({ hasDocuments }) =>
      hasDocuments ? 'DocumentConnection' : 'FolderConnection',
  },

  Document: {
    language: async ({ languageId: id }) => await resolveLanguage({ id }),
  },

  Query: {
    folder: async (_, { id, contentFilter }) => {
      const args = contentFilter;
      if (id) args.folderId = id;
      return await exports.resolveFolder(args);
    },
    document: async (_, args) => await apiDE.post('document/retrieve', args),
  },

  Mutation: {
    addDocuments: async (_, { input: { files, folderId } }) => {
      let response;
      try {
        const formData = new FormData();
        const getFile = async file => await file;
        const loadedFiles = await Promise.all(files.map(getFile));
        loadedFiles.forEach(loadedFile => {
          formData.append(
            `files`,
            loadedFile.createReadStream(),
            loadedFile.filename,
          );
        });
        formData.append('folderId', folderId);
        response = await apiDE.postFile('document/addFile', formData);
        response = morphism(taskSchema, response);
      } catch (e) {
        debugger;
      }

      return response;
    },

    addDocumentText: async (_, { input }) =>
      await apiDE.post('document/add', input),
    editDocument: async (_, { input }) =>
      await apiDE.post('document/edit', input),
    deleteDocument: async (_, { id }) =>
      await apiDE.post('document/remove', { id }),
  },
};
