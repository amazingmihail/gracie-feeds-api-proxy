const { apiDE } = require('modules/api');

exports.resolveLanguage = async args =>
  await apiDE.post('language/retrieve', args);

exports.languageResolvers = {
  Query: {
    language: async (_, args) => await apiDE.post('language/retrieve', args),
    languages: async (_, { filter }) =>
      await apiDE.post('language/list', filter),
  },
};
