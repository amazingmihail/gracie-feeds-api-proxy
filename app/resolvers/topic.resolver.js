const { apiDE } = require('modules/api');
const { resolveTopicTypes } = require('resolvers/topicType.resolver');

exports.resolveTopic = async (args) => apiDE.post('topic/retrieve', args);

exports.topicResolvers = {
  Topic: {
    topicTypes: async (_, topic) => await resolveTopicTypes({ topicId: topic.id }),
  },

  Query: {
    topic: async (_, args) => await exports.resolveTopic(args),
    topics: async (_, { filter }) => await apiDE.post('topic/list', filter),
  },
  Mutation: {
    addTopic: async (_, { input }) => await apiDE.post('topic/add', input),
    deleteTopic: async (_, args) => await apiDE.post('topic/delete', args),
  },
};
