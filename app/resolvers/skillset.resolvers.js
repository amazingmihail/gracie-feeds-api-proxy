const { apiDE } = require('modules/api');

exports.resolveSkillset = async args =>
  await apiDE.post('skillset/retrieve', args);

exports.skillsetResolvers = {
  Query: {
    skillset: async (_, args) => await exports.resolveSkillset(args),
    skillsets: async (_, { filter }) =>
      await apiDE.post('skillset/list', filter),
  },
  Mutation: {
    addSkillset: async (_, { input }) =>
      await apiDE.post('skillset/add', input),
    deleteSkillset: async (_, args) =>
      await apiDE.post('skillset/delete', args),
  },
};
