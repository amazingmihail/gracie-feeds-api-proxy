const {
  URLResolver,
  BigIntResolver,
  DateTimeResolver,
  JSONResolver,
} = require('graphql-scalars');

const { merge } = require('lodash');
const { projectResolvers } = require('resolvers/project.resolvers');
const { documentResolvers } = require('resolvers/document.resolvers');
const { skillsetResolvers } = require('resolvers/skillset.resolvers');
const { skillResolvers } = require('resolvers/skill.resolvers');
const { languageResolvers } = require('resolvers/language.resolvers');
const { taskResolvers } = require('resolvers/task.resolvers');

const resolvers = merge(
  {
    JSON: JSONResolver,
    BigInt: BigIntResolver,
    DateTime: DateTimeResolver,
    URL: URLResolver,
  },
  projectResolvers,
  documentResolvers,
  skillsetResolvers,
  skillResolvers,
  languageResolvers,
  taskResolvers,
);

module.exports = resolvers;
