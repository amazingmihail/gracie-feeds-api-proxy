const { apiDE } = require('modules/api');

exports.nameResolvers = {
  Query: {
    name: async (_, { id }) =>
      await apiDE.post('name/retrieve', { id }),
    names: async (_, { filter }) =>
      await apiDE.post('name/list', filter),
  },
  Mutation: {
    addName: async (_, { input }) =>
      await apiDE.post('name/add', input),
    deleteName: async (_, { id }) =>
      await apiDE.post('name/delete', { id }),
    setMainName: async (_, { id }) =>
      await apiDE.post('name/setMain', { id }),
  },
}