const { apiDE } = require('modules/api');
const { resolveSkillset } = require('resolvers/skillset.resolvers');

exports.skillResolvers = {
  Skill: {
    skillset: async ({ skillsetId: id }) => await resolveSkillset({ id }),
  },

  Query: {
    skill: async (_, args) => await apiDE.post('skill/retrieve', args),
    skills: async (_, { filter }) => await apiDE.post('skill/list', filter),
  },
  Mutation: {
    addSkill: async (_, { input }) => await apiDE.post('skill/add', input),
    deleteSkill: async (_, args) => await apiDE.post('skill/delete', args),
  },
};
