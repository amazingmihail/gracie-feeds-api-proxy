const { apiDE } = require('modules/api');
const { tasksPubsub, TASK_UPDATED } = require('modules/tasks-subscription');
const { withFilter } = require('apollo-server-express');
const { morphism } = require('morphism');
const { shootTaskSub } = require('modules/tasks-subscription');
const { taskSchema } = require('./_resolver-data-schemas');

exports.resolveTask = async args => {
  const response = await apiDE.post('tasks/retrieve', args);
  return morphism(taskSchema, response);
};

exports.taskResolvers = {
  Query: {
    task: async (_, { id }) => await exports.resolveTask({ id }),
    tasks: async (_, { filter }) => {
      const params = { includeMessage: true, includeParameters: true };
      if (filter.maxNumber) params.maxNumber = filter.limit;
      if (filter.offset) params.offset = filter.offset;
      if (filter.taskTypeId) params.taskTypeId = filter.typeId;

      const result = await apiDE.post('tasks/retrieve', params);
      debugger;
      return result;
    },
    taskTypes: async () => {},
  },

  Mutation: {
    cancelTask: async () => {},
    deleteTask: async () => {},
    deleteCompletedTasks: async () => {},
  },

  Subscription: {
    taskUpdated: {
      subscribe: async (...args) => {
        try {
          const initialTaskData = await exports.resolveTask(args[1].input);
          const cb = withFilter(
            (_, { input }) => {
              const iterator = tasksPubsub.asyncIterator(TASK_UPDATED);

              setTimeout(() => {
                shootTaskSub(initialTaskData);
              }, 1000);
              return iterator;
            },
            (payload, variables) => {
              return payload.id === variables.input.id;
            },
          );
          return cb.apply(null, args);
        } catch (e) {
          debugger;
        }
      },
      // resolve: payload => {
      //   const nextPayload = morphism(taskSchema, payload);
      //   return nextPayload;
      // },
    },
    tasksUpdated: {
      // resolve: payload => {
      //   const nextPayload = morphism(taskSchema, payload);
      //   return nextPayload;
      // },
      subscribe: withFilter(
        () => tasksPubsub.asyncIterator(TASK_UPDATED),
        (payload, variables) => {
          return payload.type.typeId === variables.input.typeId;
        },
      ),
    },
  },
};
