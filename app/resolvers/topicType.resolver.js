const { apiDE } = require('modules/api');
const { resolveTopic } = require('resolvers/topic.resolver');

exports.resolveTopicType = async args =>
  await apiDE.post('topicType/retrieve', args);
exports.resolveTopicTypes = async args =>
  await apiDE.post('topicType/list', args);

exports.topicTypeResolvers = {
  TopicType: {
    topic: async (_, topicType) => await resolveTopic({ id: topicType.topicId }),
  },

  Query: {
    topicType: async (_, args) => await exports.resolveTopicType(args),
    topicTypes: async (_, { filter }) =>
      await exports.resolveTopicType(filter),
  },
  Mutation: {
    addTopicType: async (_, { input }) =>
      await apiDE.post('topicType/add', input),
    deleteTopicType: async (_, args) =>
      await apiDE.post('topicType/delete', args),
  },
};
