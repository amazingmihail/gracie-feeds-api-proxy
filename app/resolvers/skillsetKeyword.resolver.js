const { apiDE } = require('modules/api');

exports.skillKeywordResolvers = {
  Query: {
    skillKeyword: async (_, { id }) =>
      await apiDE.post('skillKeyword/retrieve', { id }),
    skillKeywords: async (_, { filter }) =>
      await apiDE.post('skillKeyword/list', filter),
  },
  Mutation: {
    addSkillKeyword: async (_, { input }) =>
      await apiDE.post('skillKeyword/add', input),
    editSkillKeyword: async (_, { input }) =>
      await apiDE.post('skillKeyword/edit', input),
    deleteSkillKeyword: async (_, args) =>
      await apiDE.post('skillKeyword/delete', args),
    bulkDeleteSkillKeyword: async (_, args) =>
      await apiDE.post('skillKeyword/bulkDelete', args),
  },
};
