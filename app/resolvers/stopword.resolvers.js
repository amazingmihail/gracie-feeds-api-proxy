const { apiDE } = require('modules/api');

exports.stopWordResolvers = {
  Query: {
    stopWord: async (_, { id }) =>
      await apiDE.post('stopWord/retrieve', { id }),
    stopWords: async (_, { filter }) =>
      await apiDE.post('stopWord/list', filter),
  },
  Mutation: {
    addStowWord: async (_, { input }) =>
      await apiDE.post('stopWord/add', input),
    deleteStowWord: async (_, { id }) =>
      await apiDE.post('stopWord/delete', { id }),
  },
};
