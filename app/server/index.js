const { importSchema } = require('graphql-import');
const express = require('express');
const { ApolloServer, gql } = require('apollo-server-express');
const resolvers = require('resolvers');
const logger = require('modules/logger');
const { wsConnect } = require('modules/websocket');
const { createServer } = require('http');

const app = express();

// const typeDefs = gql(importSchema("schema/schema.graphql"));
const typeDefs = gql(importSchema('app/schema/schema.graphql'));

const server = new ApolloServer({
  typeDefs,
  resolvers,
  uploads: {
    maxFileSize: 10000000, // 10 MB
    maxFiles: 20,
  },
  // subscriptions: {
  //   onConnect: (connectionParams, webSocket) => {
  //     debugger;
  //   },
  // },
});

server.applyMiddleware({ app, path: '/graphql' });

const httpServer = createServer(app);
server.installSubscriptionHandlers(httpServer);

const startServer = () =>
  new Promise(resolve => {
    httpServer.listen({ port: process.env.APP_PORT }, () => {
      const serverPath = `http://localhost:${process.env.APP_PORT}${server.graphqlPath}`;
      logger.info(`Server started at ${serverPath}`);
      resolve(serverPath);
    });
    // app.listen({ port: process.env.APP_PORT }, () => {});
  });

const init = async () => {
  await startServer();
  wsConnect();
  logger.info(`Started websocket connection`);
};

init();
