const { importSchema } = require('graphql-import');
const { gql } = require('apollo-server-express');
const { camelCase } = require('lodash');
const prettier = require('prettier');
const fs = require('fs');
const path = require('path');
// const argv = require('yargs').argv;
const pluralize = require('pluralize');

const schema = importSchema('app/schema/schema.graphql');

const DIRECTIVES = {
  replace: 'restApiReplace',
  url: 'restApiURL',
  task: 'restApiTask',
};
const SCALAR_TYPES = [
  'String',
  'Int',
  'Float',
  'ID',
  'Boolean',
  'JSON',
  'Upload',
  'URL',
  'DateTime',
  'BigInt',
];
const ALLOWED_INPUTS = ['filter', 'options', 'input'];

const regexpIsMutationName = /^(?<mutation>(add|edit|delete|clone|setMain|bulkDelete|restore))(?<request>([A-Z].*))/;

const groupDefinitionsByTypes = defs => {
  const result = {
    Query: {},
    Mutation: {},
    scalarTypes: {},
    types: {},
    inputs: {},
    enums: {},
    unions: {},
    interfaces: {},
    all: {},
  };

  defs.forEach(def => {
    result.all[def.name.value] = def;

    switch (def.kind) {
      case 'ObjectTypeDefinition':
        if (def.name.value === 'Query') result.Query = def;
        else if (def.name.value === 'Mutation') result.Mutation = def;
        else if (def.name.value === 'Subscription') result.Subscription = def;
        else {
          result.types[def.name.value] = def;
        }
        break;

      case 'ScalarTypeDefinition':
        result.scalarTypes[def.name.value] = def;
        break;

      case 'InputObjectTypeDefinition':
        result.inputs[def.name.value] = def;
        break;

      case 'EnumTypeDefinition':
        result.enums[def.name.value] = def;
        break;

      case 'UnionTypeDefinition':
        result.unions[def.name.value] = def;
        break;

      case 'InterfaceTypeDefinition':
        result.interfaces[def.name.value] = def;
        break;

      case 'DirectiveDefinition':
        break;

      default:
        debugger;
        break;
    }
  });

  return result;
};

const getItemMeta = item => {
  const result = {
    type: '',
    isRequired: false,
    isArray: false,
    directives: null,
  };

  if (!item) {
    debugger;
  }

  if (item.directives && item.directives.length) {
    result.directives = {};
    item.directives.forEach(d => {
      result.directives[d.name.value] = null;
      if (!d.arguments) return;
      result.directives[d.name.value] = Object.fromEntries(
        d.arguments.map(arg => {
          // convert string boolean (ie "true") to boolean (ie true)
          let { value } = arg.value;
          if (value === 'true') value = true;
          if (value === 'false') value = false;

          return [arg.name.value, value];
        }),
      );
    });
  }

  try {
    if (item.type) {
      let nextType = item;

      while (nextType) {
        if (nextType.kind === 'NonNullType') result.isRequired = true;
        if (nextType.kind === 'ListType') result.isArray = true;

        if (!nextType.type) result.type = nextType;
        nextType = nextType.type;
      }
    } else {
      result.type = item;
    }
  } catch (e) {
    debugger;
  }

  result.name = result.type.name.value;

  return result;
};

const getRESTNames = (requestName, { query }) => {
  const itemMeta = getItemMeta(query);
  const result = {
    controllerName: '',
    endpointName: '',
  };

  const mutation = requestName.match(regexpIsMutationName);
  if (mutation) {
    result.controllerName = pluralize(camelCase(mutation.groups.request), 1);
    result.endpointName = camelCase(mutation.groups.mutation);
  }

  if (!mutation) {
    const isPlural = pluralize.isPlural(requestName);
    if (isPlural) {
      result.endpointName = 'list';
      result.controllerName = pluralize(camelCase(requestName), 1);
    } else {
      result.endpointName = 'retrieve';
      result.controllerName = camelCase(requestName);
    }
  }

  if (itemMeta.directives) {
    const { controller, endpoint } = itemMeta.directives.restApiURL || {};
    if (controller) result.controllerName = controller;
    if (endpoint) result.endpointName = endpoint;
  }

  return result;
};

const getTypeProps = ({ item, allTypes }) => {
  let props = {};
  const itemMeta = getItemMeta(item);
  const isScalar = SCALAR_TYPES.includes(itemMeta.name);

  if (itemMeta.isRequired) props.notNull = true;

  if (isScalar) {
    props = { ...props, type: itemMeta.name };
    // if (isTopLevel) props = { [item.name.value]: props };
    if (itemMeta.isArray) return [props];
    return props;
  }

  if (itemMeta.type.kind === 'UnionTypeDefinition') {
    props.type = 'OneOf';
    props.variants = {};
    item.types.forEach(({ name: { value: unionSubtypeName } }) => {
      if (SCALAR_TYPES.includes(unionSubtypeName)) {
        props.variants[unionSubtypeName] = unionSubtypeName;
        return;
      }
      props.variants[unionSubtypeName] = getTypeProps({
        item: allTypes[unionSubtypeName],
        allTypes,
      });
    });
    return props;
  }

  // TODO: isRequired not working for enums at the moment, thus all enums have notNull = true
  if (itemMeta.type.kind === 'EnumTypeDefinition') {
    props.type = 'Enum';
    props.notNull = true;
    props.values = item.values.map(v => v.name.value);
    return props;
  }

  if (itemMeta.type.kind === 'NamedType') {
    const nextType = {
      ...allTypes[itemMeta.name],
      parentDirectives: itemMeta.parentDirectives,
    };
    if (itemMeta.directives) {
      nextType.parentDirectives = {
        ...nextType.parentDirectives,
        [itemMeta.name]: itemMeta.directives,
      };
    }
    const nextTypeProps = getTypeProps({
      item: nextType,
      allTypes,
    });

    if (itemMeta.isArray) {
      return [nextTypeProps];
    } else {
      return nextTypeProps;
    }
  }

  if (itemMeta.type.kind === 'FieldDefinition') {
    const fieldName = item.name.value;
    props[fieldName] = isScalar
      ? itemMeta.name
      : getTypeProps({ item: allTypes[itemMeta.name], allTypes });
    return props;
  }

  if (itemMeta.type.kind === 'ObjectTypeDefinition') {
    let taskTypeReplace = [];
    if (
      item.parentDirectives &&
      item.parentDirectives.Task &&
      item.parentDirectives.Task[DIRECTIVES.task]
    ) {
      taskTypeReplace = Object.keys(
        item.parentDirectives.Task[DIRECTIVES.task],
      );
    }

    item.fields.forEach(field => {
      const fieldMeta = getItemMeta(field);
      let nextItem = field;

      // If DIRECTIVES.task - then replace task parameters
      if (taskTypeReplace.includes(field.name.value)) {
        const nextItemType =
          item.parentDirectives.Task[DIRECTIVES.task][field.name.value];

        if (SCALAR_TYPES.includes(nextItemType)) {
          props[field.name.value] = { type: nextItemType };
          return;
        } else if (nextItemType === 'null' || nextItemType === null) {
          return null;
        }
        nextItem = allTypes[nextItemType];
        props[field.name.value] = getTypeProps({ item: nextItem, allTypes });
        return;
      }

      // if DIRECTIVES.replace - then return redefined type from directive
      if (fieldMeta.directives && fieldMeta.directives[DIRECTIVES.replace]) {
        const { type, name } = fieldMeta.directives[DIRECTIVES.replace];
        if (!type || !name) return;

        const result = { type };
        if (fieldMeta.isRequired) result.notNull = true;

        if (fieldMeta.isArray) props[name] = [result];
        else props[name] = result;
        return;
      }

      props[field.name.value] = getTypeProps({ item: nextItem, allTypes });
    });

    return props;
  }

  debugger;
};

const getRESTArguments = ({ args, allTypes }) => {
  if (!args || !args.length) return null;
  let params = {};
  args.forEach(item => {
    const itemMeta = getItemMeta(item);
    const { restApiInclude } = itemMeta.directives || {};

    if (
      !SCALAR_TYPES.includes(itemMeta.type.name.value) &&
      !ALLOWED_INPUTS.includes(item.name.value) &&
      !restApiInclude
    ) {
      return;
    }

    params = {
      ...params,
      ...getTypeInputs({ item, allTypes, isTopLevel: true }),
    };
  });
  return params;
};

const getTypeInputs = ({ item, allTypes, isTopLevel }) => {
  let props = {};

  const itemMeta = getItemMeta(item);
  const isScalar = SCALAR_TYPES.includes(itemMeta.name);

  if (itemMeta.isRequired) props.isRequired = true;

  if (itemMeta.directives && itemMeta.directives.restApiDefault !== undefined) {
    props.defaultValue = itemMeta.directives.restApiDefault.value;
  } else if (item.defaultValue) {
    props.defaultValue !== item.defaultValue.value;
  }

  if (isScalar) {
    props = { ...props, type: itemMeta.name };
    if (itemMeta.isArray) props = [props];
    if (isTopLevel) props = { [item.name.value]: props };
    return props;
  }

  if (itemMeta.type.kind === 'NamedType') {
    const nextTypeProps = getTypeInputs({
      item: allTypes[itemMeta.name],
      allTypes,
    });
    const result = itemMeta.isArray ? [nextTypeProps] : nextTypeProps;
    const { restApiInclude } = itemMeta.directives || {};

    if (restApiInclude && restApiInclude.name) {
      return { [restApiInclude.name]: result };
    }
    return result;
  }

  if (itemMeta.type.kind === 'InputObjectTypeDefinition') {
    item.fields.forEach(inputField => {
      const inputFieldProps = getTypeInputs({
        item: inputField,
        allTypes,
      });

      const inputFieldMeta = getItemMeta(inputField);

      if (
        inputFieldMeta.directives &&
        inputFieldMeta.directives.restApiDefault !== undefined
      ) {
        inputFieldProps.defaultValue =
          inputFieldMeta.directives.restApiDefault.value;
      } else if (inputField.defaultValue) {
        inputFieldProps.defaultValue = inputField.defaultValue.value;
      }

      props[inputField.name.value] = inputFieldProps;
    });
    return props;
  }

  // TODO: isRequired not working for enums at the moment
  if (itemMeta.type.kind === 'EnumTypeDefinition') {
    props.type = 'Enum';
    props.values = item.values.map(v => v.name.value);
    return props;
  }
};

// const { schema, pathToSave } = argv;

function convertSchema({ schema, dest }) {
  const { definitions } = gql(schema);
  const { Query, Mutation, ...defsGrouped } = groupDefinitionsByTypes(
    definitions,
  );
  const specsREST = {};

  [...Query.fields, ...Mutation.fields].forEach(query => {
    const { controllerName, endpointName } = getRESTNames(query.name.value, {
      query,
    });

    const responseJSON = getTypeProps({
      item: query,
      allTypes: {
        ...defsGrouped.enums,
        ...defsGrouped.types,
        ...defsGrouped.unions,
      },
    });

    const requestParams = getRESTArguments({
      args: query.arguments,
      allTypes: { ...defsGrouped.enums, ...defsGrouped.inputs },
    });

    specsREST[controllerName] = {
      ...(specsREST[controllerName] || {}),
      [endpointName]: {
        responseJSON,
        requestParams,
      },
    };
  });

  fs.writeFileSync(
    path.join(__dirname, 'specs-rest.json'),
    prettier.format(JSON.stringify(specsREST), {
      parser: 'json',
    }),
  );
}

convertSchema({ schema, dest: 'schema.json' });
