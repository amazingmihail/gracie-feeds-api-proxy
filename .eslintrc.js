module.exports = {
  extends: ['airbnb-base', 'prettier', 'node'],
  env: {
    node: true,
    mocha: true,
    es6: true,
  },
  plugins: ['chai-friendly'],
  rules: {
    'import/no-commonjs': 0,
    'import/no-nodejs-modules': 0,
    'consistent-return': 0,
    'no-return-await': 0,
    'implicit-arrow-linebreak': ['off'],
    'no-underscore-dangle': 0,
    'no-shadow': 0,
    'no-multi-assign': 0,
    'no-lone-block': 0,
    'no-unused-expressions': 0,
    // from front-end
    'max-len': 0,
    'newline-per-chained-call': 0,
    'no-confusing-arrow': 0,
    'no-console': 1,
    'no-use-before-define': 0,
    eqeqeq: 0,
    'no-unused-vars': 1,
    'no-debugger': 1,
    'no-param-reassign': 1,
    'prefer-const': 1,
    'comma-dangle': 1,
    'arrow-body-style': 0,
    'func-names': 0,
    'prefer-arrow-callback': 0,
    'prefer-destructuring': 0,
    'object-shorthand': [1, 'properties'],
    'spaced-comment': 0,
    'lines-between-class-members': 0,
    'linebreak-style': 0,
    'no-bitwise': 0,
    indent: 0,
    'no-else-return': 0,
    'no-plusplus': 0,
    'no-nested-ternary': 0,
    'prefer-template': 0,
  },
  settings: {
    'import/resolver': {
      node: {
        moduleDirectory: ['node_modules', 'app'],
      },
    },
  },
};
