### Dev environment setup guide
1. Clone the repository `git clone git@gitlab.com:amazingmihail/gracie-feeds-api-proxy.git`
1. Go to the repository folder `cd gracie-feeds-api-proxy`
1. There is a submodule for schema, to pull it run `git submodule init` and follow with `git submodule update`. It should pull all the submodules, in our case a schema submodule to `app/schema`.  
1. Now install dependencies with `npm install` or `yarn install`. 
1. For development install `nodemon` globally with `npm i -g nodemon`.
1. Run dev environment with `npm run dev` or `yarn dev`. 

It is required to use `bash` or compatible line in order to execute build scripts on dependencies pulled directly from git repositories (not npm repository).  
It is recommended to use `yarn` instead of `npm`. 